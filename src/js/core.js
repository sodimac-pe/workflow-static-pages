/**
 * Here you can place all the necessary javascript core logic
 */

var $slickSlide1;

var dataArticles = [
   /*{
     "Id": 1,
     "Titulo": "Cementicios, estilo moderno industrial",
     "Url": "/sodimac-pe/search/?Ntt=cementicios%20holztek",
     "Image": "/static/contenido/holztek/dist/img/holztek--articles-2.jpg"
   },*/
   {
      "Id": 2,
      "Titulo": "Maderados, calidez para tu hogar",
      "Url": "/sodimac-pe/category/cat30072/Pisos-de-Madera/N-1z13uwb",
      "Image": "/media/images/holztek--articles-3.jpg"
   },
   {
      "Id": 3,
      "Titulo": "Marmolados, vetas que no pasan de moda",
      "Url": "/sodimac-pe/search/N-1z13uwb?Ntk=sod&Ntt=marmol+holztek",
      "Image": "/media/images/holztek--articles-4.jpg"
   },
   /*{
     "Id": 4,
     "Titulo": "Piedras, personalidad y estilo para cada rinc&oacute;n",
     "Url": "https://www.sodimac.cl/sodimac-cl/search/?Ntt=piedras%20holztek",
     "Image": "/media/images/holztek--articles-5.jpg"
   }*/
];

var dataVideos = [
   {
      "Id": 1,
      "Url": "ft32qwaZuCU",
      "Image": "/media/images/freepower-1.jpg"
   },
   {
      "Id": 2,
      "Url": "Y7uDHETTsTw",
      "Image": "/media/images/freepower-2.jpg"
   },
   {
      "Id": 3,
      "Url": "zZKTvQNMiBI",
      "Image": "/media/images/freepower-3.jpg"
   }
];

var dataArticleRandom = getRandom(dataArticles, 2);
var dataVideoRandom = getRandom(dataVideos, 0);

function getRandom(arr, n) {
   var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
   if (n > len)
      throw new RangeError("getRandom: more elements taken than available");
   while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
   }
   return result;
}

$(document).ready(function () {

   $.each(dataArticleRandom, function (i, item) {
      var id = dataArticleRandom[i]["Id"];
      var select_id = dataArticles.findIndex(x => x.Id === dataArticleRandom[i]["Id"]);

      var rowHTML =
         '<div class="row-item">\
              <a href="'+ dataArticles[select_id]["Url"] + '" target="_blank">\
              <img src="'+ dataArticles[select_id]["Image"] + '" alt="' + dataArticles[select_id]["Titulo"] + '">\
              <h3 class="title">'+ dataArticles[select_id]["Titulo"] + '</h3>\
              <p class="link">Ver productos</p>\
              </a>\
              </div>';

      $(".reference-tutoriales").append(rowHTML);
   });

   $.each(dataVideoRandom, function (i, item) {
      var id = dataVideoRandom[i]["Id"];
      var select_id = dataVideos.findIndex(x => x.Id === dataVideoRandom[i]["Id"]);

      var rowHTML = '<div class="info-video-preview__embed" data-url="' + dataVideos[select_id]["Url"] + '">\
           <img class="preview" src="'+ dataVideos[select_id]["Image"] + '" alt="Transf&oacute;rmate en un profesional del trabajo en casa.">\
               <iframe src="" class="opacity" frameborder="0" allowfullscreen></iframe>\
               </div>';

      $(".info-video-preview").append(rowHTML);
   });

   slickSlider();
   sliderholztek();
   animatedComponents("sliderIntro");

   $(".sidebar-mobile").click(function () {
      var target = $(this).parent().children(".sidebar-menu");
      $(target).slideToggle();
   });
   $(".info-video-preview__embed .preview").click(function () {
      $(".info-video-preview__embed").html("<iframe src='https://www.youtube.com/embed/" + $("body").find(".info-video-preview__embed").data("url") + "?autoplay=1&rel=0' frameborder='0' allowfullscreen></iframe>");
   });

   $(".featured").click(function () {
      $(".featured .preview").css("display", "none");
      $(".info-video-featured__embed").css("display", "block");
      $(".info-video-featured__embed").html("<iframe src='https://www.youtube.com/embed/4PXwvQg4B-A?autoplay=1&rel=0' frameborder='0' allowfullscreen></iframe>");
   });
});

$(window).resize(function (e) {
   slickSlider();
});

function sliderholztek() {
   $('.slide-1').fadeIn(1000, function () {
      setTimeout(function () {
         $('.slide-2').fadeIn(1000, function () {
            $('.slide-1').fadeOut(200, function () {
               setTimeout(function () {
                  $('.slide-2').delay(300).fadeOut(1000, function () {
                     setTimeout(function () {
                        sliderholztek();
                     }, 1500);
                  });
                  $('.slide-1').fadeIn(200);
               }, 1500);
            })
         });
      }, 1500);
   });
}

function slickSlider() {
   if ($(window).width() <= 700) {
      if (!$('.reference-ideas').hasClass('slick-initialized')) {

         $slickSlide1 = $('.reference-ideas');

         $slickSlide1.slick({
            variableWidth: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            centerMode: true,
            adaptiveHeight: true,
            dots: true,
            infinite: false,
            prevArrow: '<div class="slick-prev opacity"><svg viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve"><path class="st0" d="M6.7,11.8c-0.2-0.2-0.3-0.5-0.3-0.8c0-0.3,0.1-0.6,0.3-0.8l6.9-6.9c0.4-0.4,1.1-0.4,1.6,0c0.4,0.4,0.4,1.1,0,1.6L9.1,11l6.1,6.1c0.4,0.4,0.4,1.1,0,1.6c-0.4,0.4-1.1,0.4-1.6,0L6.7,11.8"/></svg></div>',
            nextArrow: '<div class="slick-next"><svg viewBox="0 0 22 22"><defs><clipPath><path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/></clipPath><clipPath><path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/></clipPath></defs><path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373" transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" fill="#4d4d4d"/></svg></div>'
         });

         $slickSlide1.on('afterChange', function (event, slick, currentSlide, nextSlide) {
            if (currentSlide === ($(".reference-ideas .row-item").length - 1)) {
               $('.slick-next').addClass('opacity');
            } else {
               $('.slick-next').removeClass('opacity');
            }

            if (currentSlide === 0) {
               $('.slick-prev').addClass('opacity');
            } else {
               $('.slick-prev').removeClass('opacity');
            }
         });

      }
   } else {
      if ($('.reference-ideas').hasClass('slick-initialized')) {
         $slickSlide1.slick('unslick');
      }
   }
}

function animatedComponents(id) {
   if (id == "sliderIntro") {
      setTimeout(function () {
         $(".slide__logo").css("display", "block").addClass("animated fadeInDown");
         setTimeout(function () {
            $(".slide__text").css("display", "block").addClass("animated fadeInUp");
         }, 500);
      }, 1000);
   }
}
