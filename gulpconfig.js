const project = 'holztek',
		    src = './src/',
		  build = './build/',
		   dist = './static/contenido/holztek/',
      modules = './node_modules/';

module.exports = {
	browsersync: {
		files: [ build + '/**', '!' + build + '/**.map' ],
		notify: true,
		open: true,
		port: 3002,
		//proxy: 'sodimac.local',
      server: {
         baseDir: build,
         index: 'pages/home.html',
      }
	},
   media: {
      images: {
         build: {
            src: src + '**/*(*.png|*.jpg|*.jpeg|*.gif|*.svg|*.ico|*.webp)',
            dest: build
         },
         dist: {
            src: [dist + '**/*(*.png|*.jpg|*.jpeg|*.gif|*.svg|*.ico|*.webp)', '!' + dist + 'screenshot.png'],
            imagemin: {
                optimizationLevel: 7,
                progressive: true,
                interlaced: true
            },
            dest: dist + 'media/images/',
         }
      },
      videos: {
         build: {
            src: src + '**/*(*.mp4|*.ogv|*.webm)',
            dest: build
         },
         dist: {
            src: [dist + '**/*(*.mp4|*.ogv|*.webm)'],
            dest: dist + 'media/videos/',
         }
      }
   },
	scripts: {
		src: [src + 'js/**/*.js', '!' + src + 'js/vendor/**/*'],
      srcVendor: src + 'js/vendor/**/*',
		dest: build + 'js/',
		lint: {
         src: [src + 'js/**/*.js']
      },
      minify: {
         src: build + 'js/**/*.js',
         uglify: {},
         dest: build + 'js/'
      },
      namespace: project + '-',
      dist: dist + 'js/',
	},
	styles: {
		build: {
         src: [src + 'scss/**/*.scss', '!' + src + 'scss/vendor/**/*'],
         dest: build + 'css'
      },
      dist: dist + 'css/',
      compiler: 'libsass',
      autoprefixer: {browsers: ['> 3%', 'last 2 versions', 'ie 9', 'ios 6', 'android 4']},
      minify: {safe: true},
      rubySass: {
         loadPath: [src + 'scss'],
         precision: 6,
         sourcemap: true
      },
      libsass: {
         includePaths: [src + 'scss'],
         precision: 6,
         onError: function (err) {
             return console.error(err);
         }
      },
	},
	html: {
      src: src + 'html/',
		build: {
         src: src + 'html/**/*(*.html|*.shtml)',
         dest: build + 'pages/'
      },
      dist: dist + 'pages/',
      layouts: {
         src: src + 'html/layouts/'
      },
		templates: {
         /* IMPORTANT: THE BUNDLE KEY MUST BE THE SAME AS FILE NAME */
         bundles: { 
            home: src + 'html/templates/home.html',
         }
		},
		master: src + 'html/master/master.html',
	},
   watch: {
   	src: {
         styles: src + 'scss/**/*.scss',
         scripts: src + 'js/**/*.js',
         images: src + '**/*(*.png|*.jpg|*.jpeg|*.gif|*.svg)',
         videos: src + '**/*(*.mp4|*.webm|*.ogv)',
         html: src + 'html/**/*.html',
      },
      watcher: 'browsersync'
   },
   utils: {
        clean: [build + '**/.DS_Store'],
        wipe: [dist],
        dist: {
            src: [build + '**/*', '!' + build + '**/*.map'],
            dest: dist,
            images: dist + 'media/images/',
            videos: dist + 'media/videos/',
            js: dist + 'js/',
            css: dist + 'css/',
            html: dist + 'pages/'
        },
        normalize: {
            src: modules + 'normalize.css/normalize.css',
            dest: src + 'scss/vendor/normalize',
            rename: '_normalize.scss'
        }
    },
    dist: dist
}
