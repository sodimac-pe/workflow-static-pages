// ==== UTILITIES ==== //

const gulp = require('gulp'),
   plugins = require('gulp-load-plugins')({ camelize: true }),
       del = require('del'),
      pump = require('pump'),
   replace = require('gulp-string-replace'),
   config  = require('../../gulpconfig').utils,
config_html = require('../../gulpconfig').html,
config_scripts = require('../../gulpconfig').scripts,
config_styles = require('../../gulpconfig').styles;

// Used to get around Sass's inability to properly @import vanilla CSS; see: https://github.com/sass/sass/issues/556
gulp.task('utils-normalize', () => {
  pump([
    gulp.src(config.normalize.src),
    plugins.changed(config.normalize.dest),
    plugins.rename(config.normalize.rename),
    gulp.dest(config.normalize.dest)
  ]);
});

// Totally wipe the contents of the `dist` folder to prepare for a clean build; additionally trigger Bower-related tasks to ensure we have the latest source files
gulp.task('utils-wipe', ['setup'], () => {
  return del(config.wipe)
});

// Clean out junk files after build
gulp.task('utils-clean', ['build', 'utils-wipe'], () => {
  return del(config.clean)
});

// Copy files from the `build` folder to `dist/[project]`
gulp.task('utils-dist', ['utils-clean'], () => {
  pump([
    gulp.src(config.dist.src),
    gulp.dest(config.dist.dest)
  ]);
});


gulp.task('dist-script-paths', () => {
  pump([
    gulp.src(config_scripts.dist + '**/*.js'),
    replace(/\/media\/images\/([\w|\s|-]*)\.(png|jpg|svg|webp)/g, function(replace) {
      return replace.replace(/\/media\/images\/([\w|\s|-]*)\.(png|jpg|svg|webp)/g, config.dist.images.replace(/.\//, '/') + '$1.$2')
    }),
    gulp.dest(config.dist.js)
  ])
})

gulp.task('dist-style-paths', () => {
  pump([
    gulp.src(config_styles.dist + '**/*.css'),
    replace(/\/media\/images\/([\w|\s|-]*)\.(png|jpg|svg|webp)/g, (replace) => {
      return replace.replace(/\/media\/images\/([\w|\s|-]*)\.(png|jpg|svg|webp)/g, config.dist.images.replace(/.\//, '/') + '$1.$2')
    }),
    gulp.dest(config.dist.css)
  ])
})

gulp.task('dist-html-paths', () => {
  console.log([config.dist.html, config_html.dist + '**/*.html']);
  pump([
    gulp.src(config_html.dist + '**/*.html'),
    replace(/\/media\/images\/([\w|\s|-]*)\.(png|jpg|svg|webp)/g, (replace) => {
      //.pipe(replace('/media/images', (replace) => {
      //console.log(replace);
      //return replace;
      return replace.replace(/\/media\/images\/([\w|\s|-]*)\.(png|jpg|svg|webp)/g, config.dist.images.replace(/.\//, '/') + '$1.$2')
    }),
    replace(/\/css\/([\w|\s|-]*)\.css/g, function(replace) {
      return replace.replace(/\/css\/([\w|\s|-]*)\.css/g, config.dist.css.replace(/.\//, '/') + '$1.css')
    }),
    replace(/\/js\/([\w|\s|-]*)\.js/g, function(replace) {
      return replace.replace(/\/js\/([\w|\s|-]*)\.js/g, config.dist.css.replace(/.\//, '/') + '$1.js')
    }),
    gulp.dest(config_html.dist)
  ])
  //return gulp.src(config_html.dist + '**/*.html')
    //.pipe(replace(/\/media\/images\/([\w|\s|-]*)\.(png|jpg|svg|webp)/g, (replace) => {
    //.pipe(replace('/media/images', (replace) => {
      //console.log(replace);
      //return replace;
      //return replace.replace(/\/media\/images\/([\w|\s|-]*)\.(png|jpg|svg|webp)/g, config.dist.images.replace(/.\//, '/') + '$1.$2')
    //}))
    //.pipe(replace(/\/css\/([\w|\s|-]*)\.css/g, function(replace) {
      //return replace.replace(/\/css\/([\w|\s|-]*)\.css/g, config.dist.css.replace(/.\//, '/') + '$1.css')
    //}))
    //.pipe(gulp.dest(config_html.dist));
  /*setTimeout(() => {
    pump([
      replace(/\/css\/([\w|\s|-]*)\.css/g, function(replace) {
        return replace.replace(/\/css\/([\w|\s|-]*)\.css/g, config.dist.replace(/.\//, '/') + '$1.css')
      }),
      // Replace js paths
      replace(/\/js\/([\w|\s|-]*)\.js/g, function(replace) {
        return replace.replace(/\/js\/([\w|\s|-]*)\.js/g, config.dist.replace(/.\//, '/') + '$1.js')
      }),
      
    ])
  }, 3000)*/
})

gulp.task('dist-paths', ['dist-style-paths', 'dist-script-paths', 'dist-html-paths']);