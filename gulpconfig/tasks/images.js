const gulp = require('gulp'),
      pump = require('pump'),
   plugins = require('gulp-load-plugins')({ camelize: true }),
    config = require('../../gulpconfig').media.images;

// Copy changed images from the source folder to `build` (fast)
gulp.task('images', () => {
	pump([
		gulp.src(config.build.src),
		plugins.changed(config.build.dest),
		gulp.dest(config.build.dest)
	]);
});

// Optimize images in the `dist` folder (slow)
gulp.task('images-optimize', ['utils-dist'], () => {
   pump([
      gulp.src(config.dist.src),
      plugins.imagemin(config.dist.imagemin),
      gulp.dest(config.dist.dest)
   ]);
});