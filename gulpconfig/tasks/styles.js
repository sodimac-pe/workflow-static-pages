// ==== STYLES ==== //

const gulp        = require('gulp')
  , pump          = require('pump')
  , plumber       = require('gulp-plumber')
  , gcmq          = require('gulp-group-css-media-queries')
  , gutil         = require('gulp-util')
  , plugins       = require('gulp-load-plugins')({ camelize: true })
  , postcss       = require('gulp-postcss')
  , config        = require('../../gulpconfig').styles
  , autoprefixer  = require('autoprefixer')
  , processors    = [autoprefixer(config.autoprefixer)] // Add additional PostCSS plugins to this array as needed
;

// Build stylesheets from source Sass files, autoprefix, and write source maps (for debugging) with rubySass
gulp.task('styles-rubysass', () => {
  pump([
    plugins.rubySass(config.build.src, config.rubySass),
    plumber(),
    on('error', gutil.log),
    postcss(processors),
    plugins.cssnano(config.minify),
    plugins.sourcemaps.write('./'),
    gulp.dest(config.build.dest)
  ]);
});

// Build stylesheets from source Sass files, autoprefix, and write source maps (for debugging) with libsass
gulp.task('styles-libsass', () => {
  pump([
    gulp.src(config.build.src),
    plumber(),
    plugins.sourcemaps.init(),
    plugins.sass(config.libsass),
    gcmq(),
    postcss(processors),
    plugins.cssnano(config.minify),
    plugins.sourcemaps.write('./'),
    gulp.dest(config.build.dest)
  ])
});

// Easily configure the Sass compiler from `/gulpconfig.js`
gulp.task('styles', ['styles-' + config.compiler]);