const gulp = require('gulp'),
   pump = require('pump'),
   plugins = require('gulp-load-plugins')({ camelize: true }),
   config = require('../../gulpconfig').media.videos;

// Copy changed images from the source folder to `build` (fast)
gulp.task('videos', () => {
   pump([
      gulp.src(config.build.src),
      plugins.changed(config.build.dest),
      gulp.dest(config.build.dest)
   ]);
});