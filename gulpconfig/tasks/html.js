// ==== HTML ==== //
const gulp = require('gulp'),
        fs = require('fs'),
   helpers = require('../helpers'),
     merge = require('merge-stream'),
   replace = require('gulp-string-replace'),
    rename = require('gulp-rename'),  
      pump = require('pump'),
      path = require('path'),
    pretty = require('pretty'),
    config = require('../../gulpconfig').html;

// === Home
gulp.task('html-layouts', () => {
	pump([
		gulp.src(config.src + 'layouts/*/**'),
		//gulp.src(config.src + 'layouts/**/*(*.html|*.shtml)'),
		gulp.dest(config.build.dest)
	])
});
gulp.task('html-templates-bundle', ['html-layouts'], () => {
	const tasks = [];
	for (const bundle in config.templates.bundles) {
		if (config.templates.bundles.hasOwnProperty(bundle)) {
			const ext = path.extname(config.templates.bundles[bundle]),
			     task = gulp.src(config.templates.bundles[bundle])
							.pipe(replace(new RegExp(/<!-- start:replace:content:template -->([\s\S]*?)<!-- end:replace:content:template -->/g), replace => {
								let _replace = replace.replace(/<!-- start:replace:content:template -->/g, '')
															 .replace(/<!-- end:replace:content:template -->/g, '')
															 .trim();
							   const layoutsArr = helpers.getArrayContentBetweenDoubleCurlyBraces(_replace);
							   let layoutsContent = '';
							   layoutsArr.map(layout => {
							   	const _layout = config.layouts.src + layout.replace(/@/, '').replace(/\s/g, '');
							   	const ext = fs.existsSync(_layout + '.html') ? '.html' : '.shtml';
							   	if (fs.existsSync(_layout + ext)) {
							   		layoutsContent += fs.readFileSync(_layout + ext, 'utf8');
							   	}
							   })
								return pretty(layoutsContent);
							}, { logs: { enabled: false } }))
							.pipe(gulp.dest(config.build.dest));
			tasks.push(task);
		}
	}
	return merge(tasks);
});

gulp.task('html-extends-master', ['html-templates-bundle'], () => {
	const tasks = [];
	for (var bundle in config.templates.bundles) {
		if (config.templates.bundles.hasOwnProperty(bundle)) {
			const ext = fs.existsSync(config.build.dest + bundle + '.html') ? '.html' : '.shtml';
			if (fs.existsSync(config.build.dest + bundle + ext)) {

				const bundleContent = fs.readFileSync(config.build.dest + bundle + ext, 'utf8'),
				               task = gulp.src(config.master)
								.pipe(replace(new RegExp(/<!-- @replace:content:template -->/g), replace => {
									return bundleContent;
								}, { logs: { enabled: false } }))
								.pipe(rename(bundle + ext))
								.pipe(gulp.dest(config.build.dest));
				tasks.push(task);
			}
		}
	}
	return merge(tasks);
})

gulp.task('html', ['html-extends-master']);