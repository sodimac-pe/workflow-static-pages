// ==== SCRIPTS ==== //

const gulp = require('gulp'),
   plumber = require('gulp-plumber'),
   plugins = require('gulp-load-plugins')({ camelize: true }),
      pump = require('pump'),
    config = require('../../gulpconfig').scripts;

gulp.task('scripts-lint', () => {
	pump([
		gulp.src(config.lint.src),
		plugins.jshint(),
		plugins.jshint.reporter('default')
	]);
});
gulp.task('scripts-vendor', () => {
	pump([
		gulp.src(config.srcVendor),
		plugins.sourcemaps.init(),
		plugins.babel({ 'presets': ['env'] }),
		plugins.concat(config.namespace + 'vendor.js'),
		plumber(),
		plugins.uglify(config.minify.uglify),
		plugins.sourcemaps.write('.'),
		gulp.dest(config.dest)
	]);
})
gulp.task('scripts-bundle', ['scripts-lint'], () => {
	pump([
		gulp.src(config.src),
		plugins.sourcemaps.init(),
		plugins.babel({ 'presets': ['env'] }),
		plugins.concat(config.namespace + 'core.js'),
		plumber(),
		plugins.uglify(config.minify.uglify),
		plugins.sourcemaps.write('.'),
		gulp.dest(config.dest)
	]);
});
gulp.task('scripts-minify', ['scripts-bundle'], () => {
	pump([
		gulp.src(config.minify.src),
		plugins.sourcemaps.init(),
		plumber(),
		plugins.uglify(config.minify.uglify),
		plugins.sourcemaps.write('.'),
		gulp.dest(config.minify.dest)
	]);
});

gulp.task('scripts', ['scripts-vendor', 'scripts-bundle']);