// ==== HELPERS ==== //
const fs = require('fs'),
    path = require('path'),
  pretty = require('pretty');

// Get content between double curly braces as array
exports.getArrayContentBetweenDoubleCurlyBraces = (_string) => {
	if (typeof _string !== "undefined") {
		var found = [],          // an array to collect the strings that are found
    		rxp = /{{([^}]+)}}/g,
    		curMatch;

		while( curMatch = rxp.exec( _string ) ) {
		   found.push( curMatch[1].trim() );
		}
		return found;
	}
	return [];
}

// Replace file content and write to specific or same file
exports.replaceFileContent = (readPath, search, replace, writePath) => {
	const readStream = fs.createReadStream(readPath);
	readStream.on('readable', (buffer) => {
		console.log(buffer.read().toString());
	});

	let fileContent = fs.readFileSync(readPath, 'utf8'),
	             regx = new RegExp(search),
	       regxResult = regx.exec(fileContent);

	if (regxResult) {
		fileContent = fileContent.replace(regxResult[0], replace);
		const writeStream = fs.createWriteStream(writePath);
		writeStream.once('error', (err) => {
		   console.log(err);
		});
		writeStream.write(fileContent);
		writeStream.end();
	}
}

// Create folder
exports.createFolder = (folderPath) => {
	fs.mkdirSync(folderPath, true, '777', function (err) {
  		if (err) return console.log(err);
   });
}